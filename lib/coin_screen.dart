import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'coin_domain.dart';
import 'coin_list.dart';

class CoinScreen extends StatefulWidget {
  const CoinScreen({Key? key}) : super(key: key);

  @override
  _CoinScreenState createState() => _CoinScreenState();
}

class _CoinScreenState extends State<CoinScreen> {
  CoinDomain coinDomain = new CoinDomain();
  List<Coins> coinList = [];

  @override
  void initState() {
    super.initState();

    fetchCoin();
  }

  Color colorBackground = Color(0xff000051);
  Color colorBottom = Color(0xff1a237e);

  Future<List<Coins>> fetchCoin() async {
    coinList = [];
    CoinDomain coinData;
    final response = await http.get(
        Uri.parse(
            'https://coinranking1.p.rapidapi.com/coins?referenceCurrencyUuid=yhjMzLPhuIDl&timePeriod=24h&tiers=1&orderBy=marketCap&orderDirection=desc&limit=50&offset=0'),
        headers: {
          "x-rapidapi-host": "coinranking1.p.rapidapi.com",
          "x-rapidapi-key": "8995a8cc8cmshe10cb8d52233a86p11c409jsn27b93c7616c2",
        });

    if (response.statusCode == 200) {
      List<dynamic> values = [];
      coinData = CoinDomain.fromJson(json.decode(utf8.decode(response.bodyBytes)));
      values = coinData.data!.coins!;
      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            coinList.add(values[i]);
          }
        }
        setState(() {
          coinList;
        });
      }
      return coinList;
      // return CoinDomain.fromJson(json.decode(utf8.decode(response.bodyBytes)));
    } else {
      throw Exception('Failed to load coins');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorBackground,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 25, 15, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                child: TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Search for Exchange coin',
                    hintStyle: TextStyle(fontSize: 16),
                    prefixIcon: Icon(Icons.menu),
                    suffixIcon: Icon(Icons.search),
                    border:
                        OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 35,
                decoration:
                    BoxDecoration(border: Border(bottom: BorderSide(color: Colors.white30))),
                child: Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Container(
                          padding: EdgeInsets.only(left: 50),
                          child: Text(
                            'Coin',
                            style: TextStyle(fontSize: 14, color: Colors.grey),
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: Container(
                          alignment: Alignment.centerRight,
                          child: Text(
                            'Last Price',
                            style: TextStyle(fontSize: 14, color: Colors.grey),
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: Container(
                            alignment: Alignment.centerRight,
                            child: Text('24h Change',
                                style: TextStyle(fontSize: 14, color: Colors.grey)))),
                  ],
                ),
              ),
              Expanded(
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: coinList.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return CoinList(
                        iconUrl: coinList[index].iconUrl,
                        name: coinList[index].name,
                        symbol: coinList[index].symbol,
                        btcPrice: coinList[index].btcPrice,
                        change: coinList[index].change);
                  },
                ),
              ),
              Container(
                height: 40,
                decoration: BoxDecoration(
                  color: colorBottom,
                  borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('MY WALLET',
                        style: TextStyle(
                            fontSize: 14, color: Colors.white, fontWeight: FontWeight.w500)),
                    Icon(Icons.keyboard_arrow_up, color: Colors.white),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
