import 'package:flutter/material.dart';

class CoinList extends StatefulWidget {
  final String? iconUrl;
  final String? name;
  final String? symbol;
  final String? btcPrice;
  final String? change;

  const CoinList({
    Key? key,
    required this.iconUrl,
    required this.name,
    required this.symbol,
    required this.btcPrice,
    required this.change,
  }) : super(key: key);

  @override
  _CoinListState createState() => _CoinListState();
}

class _CoinListState extends State<CoinList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.white30))),
      // color: Colors.red,
      height: 90,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 10, 10),
                  padding: EdgeInsets.all(5),
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  // color: Colors.red,
                  child: Image.network(
                    widget.iconUrl!,
                    fit: BoxFit.cover,
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 62,
                    // color: Colors.blue,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          widget.name!,
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.w500, color: Colors.white),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(widget.symbol!,
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.w400, color: Colors.grey)),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.centerRight,
              child: Text(
                widget.btcPrice!.length > 10 ? widget.btcPrice!.substring(0, 10) : widget.btcPrice!,
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.white),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  alignment: Alignment.centerRight,
                  child: Text(
                    widget.change!.substring(0, 1) != '-'
                        ? '+' + widget.change! + '%'
                        : widget.change! + '%',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: widget.change!.substring(0, 1) == '-' ? Colors.red : Colors.green),
                  ),
                ),
                Container(
                  child: widget.change!.substring(0, 1) == '-'
                      ? Icon(Icons.arrow_drop_down, color: Colors.red)
                      : Icon(Icons.arrow_drop_up, color: Colors.green),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
